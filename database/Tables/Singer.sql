﻿CREATE TABLE [dbo].[Singer]
(
	[SingerId] INT identity(1,1) NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(255) NOT NULL, 
    [GenreId] INT NULL, 
    CONSTRAINT [FK_Singer_Genre] FOREIGN KEY ([GenreId]) REFERENCES [Genre]([GenreId])
)

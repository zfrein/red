﻿CREATE TABLE [dbo].[UserInfo]
(
	[UserId] UNIQUEIDENTIFIER NOT NULL  primary key, 
	[LastName] nvarchar(50) not null,
	[FirstName] nvarchar(50) not null,
    [MobilePhone] Nvarchar(15) not NULL,
	[Email] nvarchar(50) not null
  
)

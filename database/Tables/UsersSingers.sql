﻿CREATE TABLE [dbo].[UsersSingers]
(
	[UserId] UNIQUEIDENTIFIER not null,
	[SingerId] int not null, 
    CONSTRAINT [FK_UsersSingers_User] FOREIGN KEY ([UserId]) REFERENCES [UserInfo]([UserId]), 
    CONSTRAINT [FK_UsersSingers_Singer] FOREIGN KEY ([SingerId]) REFERENCES [Singer]([SingerId]), 
    CONSTRAINT [PK_UsersSingers] PRIMARY KEY ([UserId], [SingerId])
)

﻿CREATE TABLE [dbo].[Genre]
(
	[GenreId] INT identity(1,1) NOT NULL PRIMARY KEY,
	[Name] varchar(127) not null unique
)

﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       unnamed constraint on [dbo].[UsersSingers]
     Clustered indexes that will be created
       None
     Possible data issues
       The column [dbo].[UsersSingers].[Id] is being dropped, data loss could occur.

** User actions
     Drop
       unnamed constraint on [dbo].[UsersSingers] (Primary Key)
     Alter
       [dbo].[UsersSingers] (Table)

** Supporting actions

The column [dbo].[UsersSingers].[Id] is being dropped, data loss could occur.
The project and target databases have different collation settings. Deployment errors might occur.


﻿** Highlights
     Tables that will be rebuilt
       [dbo].[UserInfo]
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The column [dbo].[UserInfo].[UserInfoId] is being dropped, data loss could occur.

** User actions
     Drop
       unnamed constraint on [dbo].[UserInfo] (Unique Constraint)
     Table rebuild
       [dbo].[UserInfo] (Table)

** Supporting actions
     Drop
       [dbo].[FK_UserInfo_UserId] (Foreign Key)
       [dbo].[FK_UsersSingers_User] (Foreign Key)
     Create
       [dbo].[FK_UserInfo_UserId] (Foreign Key)
       [dbo].[FK_UsersSingers_User] (Foreign Key)

The column [dbo].[UserInfo].[UserInfoId] is being dropped, data loss could occur.
The project and target databases have different collation settings. Deployment errors might occur.


﻿-- Region Parameters
DECLARE @0 VarChar(127) = 'Rock'
-- EndRegion
INSERT [dbo].[Genre]([Name])
VALUES (@0)
SELECT [GenreId]
FROM [dbo].[Genre]
WHERE @@ROWCOUNT > 0 AND [GenreId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 VarChar(127) = 'Pop'
-- EndRegion
INSERT [dbo].[Genre]([Name])
VALUES (@0)
SELECT [GenreId]
FROM [dbo].[Genre]
WHERE @@ROWCOUNT > 0 AND [GenreId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 VarChar(127) = 'Classik'
-- EndRegion
INSERT [dbo].[Genre]([Name])
VALUES (@0)
SELECT [GenreId]
FROM [dbo].[Genre]
WHERE @@ROWCOUNT > 0 AND [GenreId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 VarChar(127) = 'Folk'
-- EndRegion
INSERT [dbo].[Genre]([Name])
VALUES (@0)
SELECT [GenreId]
FROM [dbo].[Genre]
WHERE @@ROWCOUNT > 0 AND [GenreId] = scope_identity()


----------------------------------------------------------------------

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Rock' = [Extent1].[Name]
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = 'Depeche Mode'
DECLARE @1 Int = 1
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Rock' = [Extent1].[Name]
GO

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Rock' = [Extent1].[Name]
GO

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Pop' = [Extent1].[Name]
GO

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Pop' = [Extent1].[Name]
GO

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Pop' = [Extent1].[Name]
GO

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Pop' = [Extent1].[Name]
GO

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Classik' = [Extent1].[Name]
GO

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Classik' = [Extent1].[Name]
GO

SELECT TOP (1) 
    [Extent1].[GenreId] AS [GenreId], 
    [Extent1].[Name] AS [Name]
    FROM [dbo].[Genre] AS [Extent1]
    WHERE 'Folk' = [Extent1].[Name]
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = N'Ария'
DECLARE @1 Int = 1
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = N'Коррозия метала'
DECLARE @1 Int = 1
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = N'Иванушки Interneshenal'
DECLARE @1 Int = 2
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = N'Григорий лепс'
DECLARE @1 Int = 2
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = 'Genefer Lopez'
DECLARE @1 Int = 2
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = N'Полина Гагарина'
DECLARE @1 Int = 2
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = N'Моцарт'
DECLARE @1 Int = 3
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = N'Лист'
DECLARE @1 Int = 3
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()
GO

-- Region Parameters
DECLARE @0 NVarChar(255) = N'Бурановские бабушки'
DECLARE @1 Int = 4
-- EndRegion
INSERT [dbo].[Singer]([Name], [GenreId])
VALUES (@0, @1)
SELECT [SingerId]
FROM [dbo].[Singer]
WHERE @@ROWCOUNT > 0 AND [SingerId] = scope_identity()

﻿using System.Collections.Generic;

namespace RedCore.Services.Interface
{
    /// <summary>
    /// Сервис жанров
    /// </summary>
    public interface IGenreRepository
    {
        /// <summary>
        /// Получить все жанры.
        /// </summary>
        /// <returns></returns>
        List<Genre> GetAllGenres(redEntities context);


    }
}

﻿using System;
using RedCore.Messages;

namespace RedCore.Services.Interface
{
    /// <summary>
    /// Интерфейс сервиса информации о пользователе
    /// </summary>
    public interface IUserInfoRepository
    {
        /// <summary>
        /// Получить информацию о пользователе
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        UserInfo GetUserInfo(Guid id, redEntities context);

        /// <summary>
        /// Изменить информацию о пользователе
        /// </summary>
        /// <param name="userRequest">реквест на редактирование пользователя</param>
        /// <param name="context"></param>
        void UpdateUserInfo(UserUpdateRequest userRequest, redEntities context);

        /// <summary>
        /// Создать информацию о пользователе
        /// </summary>
        /// <param name="userRequest">реквест на создание пользователя</param>
        /// <param name="context"></param>
        void CreateUserInfo(UserUpdateRequest userRequest, redEntities context);
    }


}

﻿using System.Collections.Generic;
using RedCore.Messages;

namespace RedCore.Services.Interface
{
    /// <summary>
    /// Интерфейс сервиса исполнителей
    /// </summary>
    public interface ISingersRepository
    {
        /// <summary>
        /// Получить всех исполнителей
        /// </summary>
        /// <returns></returns>
        List<Singer> GetSingers(redEntities context);

        /// <summary>
        /// Получить всех исполнителей выбранного жанра
        /// </summary>
        /// <param name="genreId">Идентификатор жанра</param>
        /// <returns></returns>
        List<Singer> GetSingers(int genreId, redEntities context);

        /// <summary>
        /// Получить исполнителя по идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Singer GetSinger(int id, redEntities context);

        /// <summary>
        /// Создать исполнителя
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        Singer CreateSinger(SingerCreateRequest request, redEntities context);
    }
}

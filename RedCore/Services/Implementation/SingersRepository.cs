﻿using System.Collections.Generic;
using System.Linq;
using RedCore.Messages;
using RedCore.Services.Interface;

namespace RedCore.Services.Implementation
{
    /// <summary>
    /// Сервис исполнителей
    /// </summary>
    public class SingersRepository : ISingersRepository
    {
        /// <summary>
        /// Получить всех исполнителей
        /// </summary>
        /// <returns></returns>
        public List<Singer> GetSingers(redEntities context)
        {
            var dbEntities = new redEntities();
            return dbEntities.Singers.ToList();
        }

        /// <summary>
        /// Получить всех исполнителей выбранного жанра
        /// </summary>
        /// <param name="genreId">Идентификатор жанра</param>
        /// <param name="context"></param>
        /// <returns></returns>
        public List<Singer> GetSingers(int genreId, redEntities context)
        {
            return context.Singers.Where(x => x.GenreId == genreId).ToList();
        }

        /// <summary>
        /// Получить исполнителя по идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public Singer GetSinger(int id, redEntities context)
        {
            return context.Singers.FirstOrDefault(x => x.SingerId == id);
        }

        /// <summary>
        /// Создать исполнителя
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public Singer CreateSinger(SingerCreateRequest request, redEntities context)
        {
            var singer = new Singer {Name = request.Name, GenreId = request.GenreId};
            context.Singers.Add(singer);
            context.SaveChanges();
            return singer;
        }
    }
}

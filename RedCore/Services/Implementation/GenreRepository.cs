﻿using System.Collections.Generic;
using System.Linq;
using RedCore.Services.Interface;

namespace RedCore.Services.Implementation
{
   /// <summary>
   /// Сервис жанров
   /// </summary>
    public class GenreRepository : IGenreRepository
    {
        /// <summary>
        /// Получить все жанры.
        /// </summary>
        /// <returns></returns>
        public List<Genre> GetAllGenres(redEntities context)
        {
            return context.Genres.ToList();
        }
    }
}

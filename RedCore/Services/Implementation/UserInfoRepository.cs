﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using AutoMapper;
using RedCore.Messages;
using RedCore.Services.Interface;

namespace RedCore.Services.Implementation
{
    public class UserInfoRepository : IUserInfoRepository
    {
        /// <summary>
        /// Получить информацию о пользователе
        /// </summary>
        /// <param name="id"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public UserInfo GetUserInfo(Guid id,redEntities db)
        {
            return db.UserInfoes.Include("Singers").FirstOrDefault(x => x.UserId == id);

        }

        /// <summary>
        /// Изменить информацию о пользователе
        /// </summary>
        /// <param name="userRequest">реквест на редактирование пользователя</param>
        /// <param name="db"></param>
        public void UpdateUserInfo(UserUpdateRequest userRequest,redEntities db)
        {
            UserInfo userInfo = db.UserInfoes.FirstOrDefault(u => u.UserId == userRequest.UserId);

            Mapper.Map(userRequest, userInfo);
            List<Singer> forRemove =
                userInfo.Singers.Where(s => !userRequest.SingerIds.Contains(s.SingerId)).ToList();

            foreach (var singer in forRemove)
            {
                userInfo.Singers.Remove(singer);
            }
            List<Singer> forAdd = db.Singers.Where(s => userRequest.SingerIds.Contains(s.SingerId)).ToList();
            foreach (var singer in forAdd)
            {
                userInfo.Singers.Add(singer);
            }


            db.UserInfoes.AddOrUpdate((userInfo));
            db.SaveChanges();


        }

        /// <summary>
        /// Создать информацию о пользователе
        /// </summary>
        /// <param name="userRequest">реквест на создание пользователя</param>
        public void CreateUserInfo(UserUpdateRequest userRequest,redEntities db)
        {
            
            var userInfo = Mapper.Map<UserInfo>(userRequest);
            if (userRequest.SingerIds != null)
                userInfo.Singers =
                    db.Singers.Where(s => s != null && userRequest.SingerIds.Contains(s.SingerId)).ToList();
            db.UserInfoes.AddOrUpdate(userInfo);
            db.SaveChanges();
        }

    }
}

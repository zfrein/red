using System.Collections.Generic;

namespace RedCore.Messages
{
    /// <summary>
    /// ������ �� �������� ������������
    /// </summary>
    public class UserCreateRequest
    {
        /// <summary>
        /// ������� ������������
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// ��� ������������
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// ����� ����� 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// �������������� ������������.
        /// </summary>
        public List<int> SingerIds { get; set; }




    }
}
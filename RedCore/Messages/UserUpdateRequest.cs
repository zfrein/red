﻿using System;

namespace RedCore.Messages
{
    /// <summary>
    /// Запрос на изменение пользователя
    /// </summary>
    public class UserUpdateRequest : UserCreateRequest
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public Guid UserId { get; set; }

    }
}

﻿namespace RedCore.Messages
{
    /// <summary>
    /// Запрос на создание инсполнителя
    /// </summary>
    public class SingerCreateRequest
    {
        /// <summary>
        /// Имя исполнителя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор жанра
        /// </summary>
        public int? GenreId { get; set; }
        
    }
}

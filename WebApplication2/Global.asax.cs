﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;
using WebApplication2.App_Start;
using WebApplication2.Controllers;
using WebApplication2.Services.Implementation;
using WebApplication2.Services.Interface;

namespace WebApplication2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static UnityContainer container;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            container=new UnityContainer();

            container.RegisterType<IUserInfoService, UserInfoService>();
            container.RegisterType<ISingerService, SingerService>();
            container.RegisterType<IGenreService, GenreService>();
            
            container.RegisterType<RedCore.Services.Interface.IUserInfoRepository, RedCore.Services.Implementation.UserInfoRepository>();
            container.RegisterType<RedCore.Services.Interface.IGenreRepository, RedCore.Services.Implementation.GenreRepository>();
            container.RegisterType<RedCore.Services.Interface.ISingersRepository, RedCore.Services.Implementation.SingersRepository>();

            container.RegisterType<AccountController>(new InjectionConstructor());
            //container.RegisterType<RolesAdminController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());
            //container.RegisterType<UsersAdminController>(new InjectionConstructor());

            var controllerFactory = new UnityControllerFactory(container);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            InitializeAutoMapper.Initialize();
        }
    }


}

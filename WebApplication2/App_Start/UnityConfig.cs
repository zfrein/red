using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;
using WebApplication2.Controllers;
using WebApplication2.Services.Implementation;
using WebApplication2.Services.Interface;

namespace WebApplication2
{


    /// <summary>
    /// ������ ������� ������������ ������ ����������� Dependency Injection
    /// </summary>
    public class UnityControllerFactory : DefaultControllerFactory
    {
        private readonly IUnityContainer container;

        public UnityControllerFactory(IUnityContainer container)
        {
            this.container = container;

            foreach (var controllerType in Assembly.GetExecutingAssembly().GetTypes().Where(t => typeof(IController).IsAssignableFrom(t)))
            {
              //  if (!(controllerType == typeof(AccountController) || controllerType == typeof(ManageController)))
                    container.RegisterType(controllerType, controllerType.FullName);
            }
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException((int)HttpStatusCode.NotFound,
                    String.Format(
                        CultureInfo.CurrentCulture,
                        "The resource {0} cannot be found",
                        requestContext.HttpContext.Request.Path));
            }
            return (IController)this.container.Resolve(controllerType);
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using AutoMapper;
using RedCore;
using RedCore.Messages;
using WebApplication2.Models;


namespace WebApplication2.App_Start
{
    public static class InitializeAutoMapper
    {
        public static void Initialize()
        {
            CreateModelsToViewModels();
            CreateViewModelsToModels();
        }

        private static void CreateModelsToViewModels()
        {
            Mapper.CreateMap<HashSet<SingerModel>, HashSet<Singer>>();

            Mapper.CreateMap<UserInfoModel, UserInfo>();
                //.ForMember(dest => dest.Singers, m => m.MapFrom(a => a.Singers.Select(Mapper.Map<SingerModel, Singer>)));

            Mapper.CreateMap<UserInfoViewModel, UserInfo>();
                //.ForMember(dest => dest.Singers, m => m.MapFrom(a => a.Singers.Select(Mapper.Map<SingerModel, Singer>)
                


            Mapper.CreateMap<SingerModel, Singer>();
            Mapper.CreateMap<GenreModel, Genre>();

            Mapper.CreateMap<UserUpdateRequest, UserInfo>().ReverseMap();

        }

        private static void CreateViewModelsToModels()
        {
            Mapper.CreateMap<UserInfo, UserInfoModel>();
            
                //.ForMember(dest => dest.Singers, m => m.MapFrom(a => a.Singers.Select(Mapper.Map<Singer, SingerModel>).ToList()));
            Mapper.CreateMap<UserInfo, UserInfoViewModel>();
                
                //.ForMember(dest => dest.Singers, m => m.MapFrom(a => a.Singers.Select(Mapper.Map<Singer, SingerModel>).ToList()));
            Mapper.CreateMap<Genre, GenreModel>();
            Mapper.CreateMap<Singer, SingerModel>();
            Mapper.CreateMap<HashSet<Singer>, HashSet<SingerModel>>();

        }
    }

}
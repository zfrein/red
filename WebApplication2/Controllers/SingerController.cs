﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using WebApplication2.Services.Interface;

namespace WebApplication2.Controllers
{
    public class SingerController : Controller
    {
        private readonly ISingerService singerService;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.Mvc.Controller"/> class.
        /// </summary>
        public SingerController(ISingerService singerService)
        {
            this.singerService = singerService;
        }


        [Authorize]
        public ActionResult AddNewFavoriteSinger()
        {
            return PartialView("SingerCreateView", this.singerService.GetCreateViewModel());
        }

        [Authorize]
        
        public JsonResult CreateNewSinger(int? genreId, string name)
        {
            
            return Json( this.singerService.CreateNewSinger(name, genreId));
        }

        
        public JsonResult GetByGenre(int genreId)
        {
            List<SingerModel> singerByGenre = this.singerService.GetSingerByGenre(genreId);

            return Json(singerByGenre);
        }

    }
}
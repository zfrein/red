﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WebApplication2.Models;
using WebApplication2.Services.Interface;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        private IUserInfoService userInfoService;

        public HomeController(IUserInfoService userInfoService)
        {
            this.userInfoService = userInfoService;
        }

        [Authorize]
        public ActionResult Index()
        {
            return RedirectToAction("ViewUser");
        }

        [Authorize]
        public ActionResult ViewUser()
        {
            var userInfoModel = this.userInfoService.GetUserInfoModel(this.GetUserId());
            return View("ViewUser", userInfoModel);
        }

        [Authorize]
        public ActionResult EditUser()
        {
            var userInfoViewModel = this.userInfoService.GetUserInfoForEdit(this.GetUserId());
            return this.View(userInfoViewModel);
        }

        /// <summary>
        /// Получить идентификатор текущего пользователя
        /// </summary>
        /// <returns></returns>
        private Guid GetUserId()
        {
            return new Guid(this.User.Identity.GetUserId());
        }


        [Authorize]
        [HttpPost]
        public ActionResult SaveUser(UserInfoViewModel viewModel)
        {
            this.userInfoService.SaveUserInfo(viewModel);
            return RedirectToAction("EditUser");
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateUser(UserInfoViewModel viewModel)
        {
            viewModel.UserId = this.GetUserId();
            this.userInfoService.CreateUserInfo(viewModel);
            return RedirectToAction("EditUser");
        }

        [Authorize]
        public ActionResult RemoveFavoriteSinger(int id)
        {
            this.userInfoService.RemoveFavoriteSinger(id, this.GetUserId());

            return new EmptyResult();
        }


        [Authorize]
        public ActionResult AddFavoriteSinger(int id)
        {
            return PartialView("~/Views/Shared/SingerEditView.cshtml", this.userInfoService.AddFavoriteSinger(id, this.GetUserId()));
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using RedCore;

namespace WebApplication2.Models
{
    public class UserInfoModel
    {
        public Guid UserId { get; set; }

         [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Поле должно быть установлено")]
        public string FirstName { get; set; }

        [Display(Name = "Мобильный телефон")]
        [Required(ErrorMessage = "Поле должно быть установлено")]
        [RegularExpression(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$",ErrorMessage = "Не корректный номер")]      
        public string MobilePhone { get; set; }

        [Required(ErrorMessage = "Поле должно быть установлено")]
        [RegularExpression(@"^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$", ErrorMessage = "Не корректная почта")]      
        public string Email { get; set; }

        public  List<SingerModel> Singers { get; set; }
    }

    /// <summary>
    /// Вью модель для редактирования пользователя.
    /// </summary>
    public class UserInfoViewModel : UserInfoModel
    {
        public List<SelectListItem> Genres { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Models
{
    public class SingerCreateViewModel
    {
        public List<SelectListItem> Genres { get; set; }

        public string Name { get; set; }

        public Guid Id {get ; set; }
        
    }
}
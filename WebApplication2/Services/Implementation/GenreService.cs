﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using RedCore;
using WebApplication2.Models;
using WebApplication2.Services.Interface;

namespace WebApplication2.Services.Implementation
{
    public class GenreService : IGenreService
    {
        private RedCore.Services.Interface.IGenreRepository genreRepository;
        public GenreService(RedCore.Services.Interface.IGenreRepository genreRepository)
        {
            this.genreRepository = genreRepository;
        }

        public List<GenreModel> GetGenres()
        {
            using (var db=new redEntities())
            {
                return this.genreRepository.GetAllGenres(db).Select(Mapper.Map<GenreModel>).ToList();    
            }
            
        }
    }
}
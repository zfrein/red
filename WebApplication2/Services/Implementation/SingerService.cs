﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using RedCore;
using RedCore.Messages;
using RedCore.Services.Interface;
using WebApplication2.Models;
using WebApplication2.Services.Interface;

namespace WebApplication2.Services.Implementation
{
    public class SingerService : ISingerService
    {
        private readonly ISingersRepository singerRepository;
        private readonly IGenreService genreService;
        public SingerService(ISingersRepository singersRepository, IGenreService genreService)
        {
            this.singerRepository = singersRepository;
            this.genreService = genreService;
        }

        public List<SingerModel> GetSingerByGenre(int genreId)
        {
            using (var db = new redEntities())
            {
                return Enumerable.ToList(this.singerRepository.GetSingers(genreId, db).Select(Mapper.Map<SingerModel>));
            }
        }

        public SingerCreateViewModel GetCreateViewModel()
        {
            return new SingerCreateViewModel
                {
                    Genres =
                        genreService.GetGenres()
                            .Select(x => new SelectListItem { Value = x.GenreId.ToString(), Text = x.Name })
                            .ToList(),
                    Id = Guid.NewGuid()

                };
        }

        public SingerModel CreateNewSinger(string name, int? genreId)
        {
            using (var db = new redEntities())
            {
                return
                    Mapper.Map<SingerModel>(
                        this.singerRepository.CreateSinger(new SingerCreateRequest { GenreId = genreId, Name = name }, db));
            }
        }
    }
}
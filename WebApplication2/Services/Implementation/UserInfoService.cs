﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using RedCore;
using RedCore.Messages;
using RedCore.Services.Interface;
using WebApplication2.Models;
using IUserInfoService = WebApplication2.Services.Interface.IUserInfoService;

namespace WebApplication2.Services.Implementation
{

    /// <summary>
    /// Сервис пользователя.
    /// </summary>
    public class UserInfoService : IUserInfoService
    {
        private readonly RedCore.Services.Interface.IUserInfoRepository userRepository;
        private readonly IGenreRepository genreRepository;
        private readonly ISingersRepository singersRepository;

        public UserInfoService(RedCore.Services.Interface.IUserInfoRepository userRepository, IGenreRepository genreRepository, ISingersRepository singersRepository)
        {
            this.userRepository = userRepository;
            this.genreRepository = genreRepository;
            this.singersRepository = singersRepository;
        }

        /// <summary>
        /// Получить модель пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserInfoModel GetUserInfoModel(Guid userId)
        {
            using (var db = new redEntities())
            {

                UserInfo userInfo = this.userRepository.GetUserInfo(userId,db);
                var userInfoModel = Mapper.Map<UserInfoModel>(userInfo);
                return userInfoModel;
            }
        }

        /// <summary>
        /// Получить вью модель для редактирования вользователя.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserInfoViewModel GetUserInfoForEdit(Guid userId)
        {
            using (var db = new redEntities())
            {
                var userInfo = this.userRepository.GetUserInfo(userId,db);
                UserInfoViewModel userViewModel;
                if (userInfo != null)
                    userViewModel = Mapper.Map<UserInfoViewModel>(userInfo);
                else

                    userViewModel = new UserInfoViewModel();

                userViewModel.Genres =
                    this.genreRepository.GetAllGenres(db)
                        .Select(g => new SelectListItem { Text = g.Name, Value = g.GenreId.ToString() })
                        .ToList();
                return userViewModel;
            }
        }

        /// <summary>
        /// Сохранить инзменную информацию о пользователе.
        /// </summary>
        /// <param name="model"></param>
        public void SaveUserInfo(UserInfoViewModel model)
        {
            using (var db = new redEntities())
            {
                var userInfo = this.userRepository.GetUserInfo(model.UserId,db);
                var req = new UserUpdateRequest
                {
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    MobilePhone = model.MobilePhone,
                    UserId = model.UserId,
                    SingerIds = userInfo.Singers.Select(x => x.SingerId).ToList()
                };

                this.userRepository.UpdateUserInfo(req,db);
            }
        }

        /// <summary>
        /// Сохранить новую информацию о пользователе.
        /// </summary>
        /// <param name="model"></param>
        public void CreateUserInfo(UserInfoViewModel model)
        {
            using (var db = new redEntities())
            {
                var req = new UserUpdateRequest
                {
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    MobilePhone = model.MobilePhone,
                    UserId = model.UserId
                };

                this.userRepository.CreateUserInfo(req,db);
            }
        }

        /// <summary>
        /// Удалить исполнителя из списка любимых
        /// </summary>
        /// <param name="id"></param>
        public void RemoveFavoriteSinger(int id, Guid userId)
        {
            using (var db = new redEntities())
            {
                var userInfo = this.userRepository.GetUserInfo(userId,db);

                var req = new UserUpdateRequest
                {
                    Email = userInfo.Email,
                    FirstName = userInfo.FirstName,
                    LastName = userInfo.LastName,
                    MobilePhone = userInfo.MobilePhone,
                    UserId = userInfo.UserId,
                    SingerIds = userInfo.Singers.Where(s => s.SingerId != id).Select(x => x.SingerId).ToList()
                };

                this.userRepository.UpdateUserInfo(req,db);
            }
        }

        public SingerModel AddFavoriteSinger(int id, Guid userId)
        {
            using (var db = new redEntities())
            {
                var userInfo = this.userRepository.GetUserInfo(userId, db);

                if (userInfo.Singers.All(s => s.SingerId != id))
                {
                    var singerIds = userInfo.Singers.Select(x => x.SingerId).ToList();

                    singerIds.Add(id);
                    var req = new UserUpdateRequest
                    {
                        Email = userInfo.Email,
                        FirstName = userInfo.FirstName,
                        LastName = userInfo.LastName,
                        MobilePhone = userInfo.MobilePhone,
                        UserId = userInfo.UserId,
                        SingerIds = singerIds
                    };

                    this.userRepository.UpdateUserInfo(req, db);
                    var singer = this.singersRepository.GetSinger(id, db);
                    var addFavoriteSinger = Mapper.Map<SingerModel>(singer);
                    return addFavoriteSinger;
                }
                return null;
            }
        }
    }
}
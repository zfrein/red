﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Services.Interface
{
    public interface ISingerService
    {
        List<SingerModel> GetSingerByGenre(int genreId);

        SingerCreateViewModel GetCreateViewModel();
        SingerModel CreateNewSinger(string name, int? genreId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Services.Interface
{
    /// <summary>
    /// Сервис для работы с жанравми
    /// </summary>
    public interface IGenreService
    {
        List<GenreModel> GetGenres();
    }
}

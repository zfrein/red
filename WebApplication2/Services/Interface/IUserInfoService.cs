﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Services.Interface
{
    /// <summary>
    /// Сервис для работы с пользователями.
    /// </summary>
    public interface IUserInfoService
    {
        /// <summary>
        /// Получить модель пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserInfoModel GetUserInfoModel(Guid userId);

        /// <summary>
        /// Получить вью модель для редактирования вользователя.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserInfoViewModel GetUserInfoForEdit(Guid userId);

        /// <summary>
        /// Сохранить инзменную информацию о пользователе.
        /// </summary>
        /// <param name="model"></param>
        void SaveUserInfo(UserInfoViewModel model);

        /// <summary>
        /// Сохранить новую информацию о пользователе.
        /// </summary>
        /// <param name="model"></param>
        void CreateUserInfo(UserInfoViewModel model);

        /// <summary>
        /// Удалить исполнителя из списка любимых
        /// </summary>
        /// <param name="id"></param>
        void RemoveFavoriteSinger(int id, Guid userId);

        /// <summary>
        /// Добавить исполнителя в список любимых
        /// </summary>
        /// <param name="id"></param>
        SingerModel AddFavoriteSinger(int id, Guid userId);

    
    }


}

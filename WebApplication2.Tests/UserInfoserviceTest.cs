﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RedCore;
using RedCore.Messages;
using RedCore.Services.Interface;
using WebApplication2.App_Start;
using WebApplication2.Models;
using WebApplication2.Services.Implementation;
using WebApplication2.Services.Interface;

namespace WebApplication2.Tests
{
    [TestClass]
    public class UserInfoserviceTest
    {
        private Mock<IUserInfoRepository> userRepository;
        private Mock<ISingersRepository> singeRepository;
        private Mock<IGenreRepository> genreRepository;

        private List<UserInfo> savedList=new List<UserInfo>(); 

        private Guid userId = new Guid("5E621574-32E0-41B4-B58F-9550B7FB2833");
        public UserInfoserviceTest()
        {
            userRepository = new Mock<IUserInfoRepository>();
            userRepository.Setup(framework => framework.GetUserInfo(It.IsAny<Guid>(), It.IsAny<redEntities>()))
                .Returns(new UserInfo { UserId = userId });

            singeRepository=new Mock<ISingersRepository>();

            genreRepository=new Mock<IGenreRepository>();
            genreRepository.Setup(r => r.GetAllGenres(It.IsAny<redEntities>())).Returns(new List<Genre> {new Genre()});

            InitializeAutoMapper.Initialize();
        }

        [TestMethod]
        public void TestGetUserInfoModel()
        {
            IUserInfoService userInfoService=new UserInfoService(userRepository.Object,genreRepository.Object,singeRepository.Object);
            var userInfoModel = userInfoService.GetUserInfoModel(userId);

            Assert.AreEqual(userId,userInfoModel.UserId);
        }

        [TestMethod]
        public void TestGetUserInfoForEdit()
        {
            IUserInfoService userInfoService = new UserInfoService(userRepository.Object, genreRepository.Object, singeRepository.Object);
            var userInfoModel = userInfoService.GetUserInfoForEdit(userId);

            Assert.AreEqual(userId, userInfoModel.UserId);
            Assert.IsNotNull(userInfoModel.Genres);
        }


        [TestMethod]
        public void TestSaveUserInfo()
        {
            IUserInfoService userInfoService = new UserInfoService(userRepository.Object, genreRepository.Object, singeRepository.Object);
             userInfoService.SaveUserInfo(new UserInfoViewModel());

            userRepository.Verify(r => r.UpdateUserInfo(It.IsAny<UserUpdateRequest>(), It.IsAny<redEntities>()));
            
            
        }
    }
}
